import "fullpage.js";
var Papa = require("papaparse");
var jQuery = require("jquery");

import grd from 'grd/grd.css';
import fullpage from 'fullpage.js/dist/jquery.fullpage.css';
import style from './style.css';


// Varible that stores id that it what state the ban is, bellow is the color of that state.
// Second element is the color of the second element.
// eg Statutory Ban is #e6ee9c
var colourslaw = {
  id: ["No Law", "Statutory Ban", "Constitutional Ban", "Legal"],
  colour: ["#D3D3D3", "#e6ee9c", "#ef9a9a", "#c5e1a5"]
};
//init fullpage
$(document).ready(function() {
  $("#fullpage").fullpage();
});
//stroe results
var csvresults;
// date starts at second element
var datenum = 2;
//read csv
Papa.parse("./ssm.csv", {
  delimiter: "", // auto-detect
  newline: "", // auto-detect
  quoteChar: '"',
  header: false,
  dynamicTyping: false,
  preview: 0,
  encoding: "",
  worker: false,
  comments: false,
  download: true,
  skipEmptyLines: true,
  complete: function(results) {
    csvresults = results;
    new Vue({
      el: "#table",
      data: {
        data: results.data
      }
    });

    new Vue({
      el: "#colourtable",
      data: {
        data: colourslaw
      }
    });

    // On complete say every two seconds update map
    // map function is used to update, second varible is the time to update
    // 2000 is 2 seconds
    setInterval(map, 2000);
  }
});

function map() {
  var visualdate = csvresults.data[0][datenum];
  //update date text
  updatedate(visualdate);
  //update map colours
  updatemap(visualdate);
  //now done add begin move to next date
  updatecomplete();
}
//update date
function updatedate(visualdate) {
  document.getElementById("ssm-date").innerHTML = visualdate;
}
//change map
function updatemap(visualdate) {
  for (let index = 1; index < csvresults.data.length; index++) {
    //get elements
    var elementid = csvresults.data[index][1];
    var elementstanding = csvresults.data[index][datenum];
    //find element index by the standing
    var colourid = colourslaw.id.findIndex(elem => elem === elementstanding);
    //then get colour by the index we just recieved
    var elementscolour = colourslaw.colour[colourid];
    //update colour
    document.getElementById(elementid).style.fill = elementscolour;
  }
}

//now complete +1 to date
function updatecomplete() {
  if (datenum == csvresults.data[0].length - 1) {
    datenum = 2;
  } else {
    datenum = datenum + 1;
  }
}
